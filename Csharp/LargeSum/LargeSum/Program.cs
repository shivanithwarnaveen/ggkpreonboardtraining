﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LargeSum
{
    class Program
    {
        public static String Addition(String value1, String value2)
        {
            StringBuilder result = new StringBuilder();
            int i = value1.Length-1;
            int i2 = value2.Length - 1;
            int carry = 0;

            for (i = value1.Length - 1, i2 = value2.Length - 1, carry = 0; i >= 0 || i2 >= 0 || carry != 0; i--, i2--)
            {
                int FirstDigit = 0;
                if(i<0)
                {
                    FirstDigit = 0;
                }
                else
                {
                    FirstDigit = (int)value1[i]-'0';
                }
                int SecondDigit = 0;
                if(i2<0)
                {
                    SecondDigit = 0;
                }
                else
                {
                    SecondDigit = (int)value2[i2]-'0';
                }
                int Digit = FirstDigit + SecondDigit + carry;
                if(Digit>9)
                {
                    carry = 1;
                    Digit = Digit - 10;
                }
                else
                {
                    carry = 0;
                }

                result.Append(Digit);
            }
            StringBuilder newResult = new StringBuilder();
            //Reversing the output
            for(i = result.Length - 1; i >= 0; i--)
            {
                newResult.Append(result[i]);
            }

            return newResult.ToString();
        }
        static void Main(string[] args)
        {
            String value1 = Console.ReadLine();
            String value2 = Console.ReadLine();
            //Console.WriteLine((int)value2[3]-'0');
            Console.WriteLine(Addition(value1, value2));
            Console.ReadKey();
        }
    }
}
