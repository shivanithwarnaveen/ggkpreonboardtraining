﻿using System;
using System.Collections.Generic;
using System.Text;
namespace TreeSort
{
    class Node
    {
        public int item;
        public Node left;
        public Node right;
    }
    class Tree
    {
        public Node root=null;
       // public void Tre()
        //{
           // root = null;
        //}
        public Node ReturnRoot()
        {
            return root;
        }
        public void Search(Node Root, int el)
        {
            //Node now=Root;
            if (Root == null)
                Console.Write("element was not found :");
            else
            {
                if (Root.item == el)
                    Console.Write("element was found:");
                else if (el > Root.item)
                    Search(Root.right, el);
                else
                    Search(Root.left, el);
            }
        }
        public void Insert(int id)
        {
            Node newNode = new Node();
            newNode.item = id;
            if (root == null)
                root = newNode;
            else
            {
                Node current = root;
                Node parent;
                while (true)
                {
                    parent = current;
                    if (id < current.item)
                    {
                        current = current.left;
                        if (current == null)
                        {
                            parent.left = newNode;
                            return;
                        }
                    }
                    else
                    {
                        current = current.right;
                        if (current == null)
                        {
                            parent.right = newNode;
                            return;
                        }
                    }
                }
            }
        }
        public void Inorder(Node Root)
        {
            if (Root != null)
            {
                Inorder(Root.left);
                Console.Write(Root.item + " ");
                Inorder(Root.right);
            }
        }
        public void desc(Node Root)
        {
            if (Root != null)
            {
                desc(Root.right);
                Console.Write(Root.item + " ");
                desc(Root.left);
            }
        }
    }
    public class Program
    {
        public static void Main(String[] args)
        {
            Tree Obj = new Tree();
            int i = 10, key;
            //for (i = 1; i < 10; i++)
            // Obj.Insert(i);
            Console.WriteLine("Enter Number of nodes u would like to insert:");
         
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter elements:");
            for (i = 0; i < n; i++)
            {
                int num= Convert.ToInt32(Console.ReadLine());
                Obj.Insert(num) ;
            }
            Console.WriteLine("ascending order ");
            Obj.Inorder(Obj.ReturnRoot());
            Console.WriteLine(" ");
            Console.WriteLine("descending order ");
            Obj.desc(Obj.ReturnRoot());
            Console.WriteLine();
            Console.WriteLine("Enter element to search");
           key = Convert.ToInt32(Console.ReadLine());
            Obj.Search(Obj.ReturnRoot(),key);
            Console.WriteLine(" ");
            //Console.WriteLine();
          //  Console.ReadLine();
            Console.ReadKey();
        }
    }
}

